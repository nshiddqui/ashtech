<style>
    .bg_overview {
        background: url("/img/about/banner-02.jpg") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }

    .card {
        background: #fff;
        border-radius: 2px;
        display: inline-block;
        text-align: center;
        width: 100%;
        position: relative;
        padding: 10px;
        margin-top: 10px;
        margin-top: 30px;
    }

    .card-1 {
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
    }

    .card-1:hover {
        box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
    }
</style>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <?= $this->Html->image('home/logo.png') ?>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <div class="col-12 mt-3">
        <div class="container">
            <h2 class="fs-2 bold">Awards & Certifications</h2>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-1.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-1.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-2.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-2.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-3.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-3.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-4.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-4.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-5.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-5.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-6.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-6.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-7.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-7.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-10.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-10.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-8.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-8.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
                <div class="col-md-6 mt-3">
                    <a href="/img/about/certificate-9.png" data-fancybox>
                        <?= $this->Html->image('about/certificate-9.png', ['style' => 'width:100%;border: 3px solid black;']) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>