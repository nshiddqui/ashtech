<style>
    .bg_overview {
        background: url("img/bricks/IMG_7955.JPG") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Bricks , Blocks & Tiles</h1>
                    <p class="white fs-2 medium">
                       "the production of Fly Ash based Bricks, Blocks and other products is 40 million pieces per annum, which the Company is planning to enhance in very near future."
                    </p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Brick Blocks Tiles</h2> -->
            <p class="fs-3 mt-5">
                With the success of each business vertical, the Company started introducing new verticals under the umbrella of Ashtech Group. One of its ventures is in <b>manufacturing of ISI Marked Fly Ash Bricks, Blocks & Allied products</b>. This new business <b>vertical started operations in 2006 at Dadri, Greater Noida, G.B. Nagar (U.P.)</b>. Since inception, the new venture has been achieving new milestones with each passing year.
            </p>
            <p class="fs-3 mt-5">
                Presently, the <b>production of Fly Ash based Bricks, Blocks and other products is 40 million pieces per annum</b>, which the Company is planning to enhance in very near future.
            </p>
            <h2 class="fs-2 my-5 bold" style="text-decoration: underline;">Gallery</h2>
            <div class="row">
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_7939.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_7939.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_7961.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_7961.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_8016.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_8016.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_8012.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_8012.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_8001.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_8001.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_7934.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_7934.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/img/bricks/IMG_7933.JPG" data-fancybox>
                        <?= $this->Html->image('bricks/IMG_7933.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>