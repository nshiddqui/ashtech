<style>
    .bg_overview {
        background: url("/img/about/banner-02.jpg") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }
    * {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}


    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Careers</h1>
                    <p>Be-aware of fake jobs , <strong>Ash Tech </strong> does not send job offers from free internet email services like Gmail, Rediffmail, Yahoo mail, Hotmail, and so on</p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec row no-gutters sec_py fjobcont">
<div class="container">
  <form action="home.php">
    <label for="fname">First Name</label>
    <input type="text" id="fname" name="firstname" placeholder="Your name..">

    <label for="lname">Last Name</label>
    <input type="text" id="lname" name="lastname" placeholder="Your last name..">

    <label for="country">Country</label>
    <select id="country" name="country">
      <option value="australia">Australia</option>
      <option value="canada">Canada</option>
      <option value="usa">USA</option>
    </select>

    <label for="subject">Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

    <input type="submit" value="Submit">
  </form>
</div>
    <!-- <div class="col-12">
        <div class="container">
            <h2 class="fs-2 bold pl-5">Caution Notice</h2>
            <div class="bg_white px-5 py-3">
                <p class="fs-3">
                    This is to notify to the public that some unscrupulous persons posing themselves as employees/representatives/agents of Ashtech  (and its associated/group companies), with ulterior motive to earn wrongful gain and/or cheat the prospective job seekers and students, are fraudulently offering jobs online on certain websites/or through telephone calls or fake letters, soliciting them for jobs in Ashtech , and are asking them to deposit some amount in certain bank accounts. These people are also unauthorisedly using the name, trademark, domain name and logo of Ashtech  with a view to tarnish the image and reputation of Ashtech .
                </p>
                <p class="fs-3 mt-5">
                    We wish to caution the general public that Ashtech  (and its associated/group companies) have neither appointed nor authorized any person or agency to offer jobs to people online on such websites, or to issue any letters or act on our behalf or use the name, trademark and logo/websites.
                </p>
                <p class="fs-3 mt-5">
                    The public in general are also advised not to fall prey to such fraudulent activities. The Ashtech  (and its associated/group companies) do not ask, solicit and accept any monies in any form from the candidates/job applicants/potential job seekers, who have applied or wish to apply to us, whether online or otherwise as a pre-employment requirement. Ashtech  (and its associated/group companies) bears no responsibility for amounts being deposited / withdrawn there from in response to such Offers.

                </p>
                <p class="fs-3 mt-5">
                    It may be noted that Ashtech  (and its associated/group companies) follows a formal recruitment process through its own HR department and does not outsource the final selection of prospective employees to any individuals or agencies. Anyone dealing with such fake interview calls would be doing so at his / her own risk and the Ashtech  or its associated/group companies will not be held responsible for any loss or damage suffered by such persons, directly or indirectly. The company strongly recommends that the potential job-seekers should not respond to such solicitations.
                </p>
                <p class="fs-3 mt-5">
                    Should you come across any such fraudulent incident or have any information regarding solicitation for recruitment or employment with Ashtech  (and its associated/group companies), please assist us in taking appropriate action to curb such mala fide activities.
                </p>
            </div>
        </div>
    </div> -->
</div>