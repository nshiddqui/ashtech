<style>
    .bg_overview {
        background: url("/img/about/banner-02.jpg") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }

    .card {
        background: #fff;
        border-radius: 2px;
        display: inline-block;
        text-align: center;
        width: 100%;
        position: relative;
        padding: 10px;
        margin-top: 10px;
        margin-top: 30px;
    }

    .card-1 {
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
    }

    .card-1:hover {
        box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
    }
</style>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <?= $this->Html->image('home/logo.png') ?>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Our Clients</h2> -->
            <p class="fs-3 mt-5">
                The Company is associated with several esteemed organizations:
            </p>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/landt.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/apco.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/samindia.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/oriental.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/jaiprakash.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/sadbhev.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <!-- <div class="col-md-4">
                    <div class="card card-1">
                        <h5>PSY Infrastructure Pvt. Ltd.</h5>
                        <p>(Governments Contractors)</p>
                    </div>
                </div> -->
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/pnc.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <!-- <div class="col-md-4">
                    <div class="card card-1">
                        <h5>SEC Buildtech Pvt. Ltd.</h5>
                        <p>(Governments Contractors)</p>
                    </div>
                </div> -->
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/toshiba.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/ahliwalia.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/samsung.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/oppo.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/sri.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/ultratech.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/acc.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/prism.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/hil.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/everest.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/nuvoco.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/gaurs.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/wave.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/tags.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/ats.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/mahagun.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/gulshan.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/pigeon.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/anjara.jpg" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/cds.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1">
                        <img src="webroot/img/logos/uprnn.png" style="width: 330px; height:111px">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-1" style="height: 133px">
                        <h5 style="margin-top: 40px;font-weight: bold;">And others various clients</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>