<style>
    .bg_overview {
        background: url("img/crawler.jpg") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Construction Equipment Rental</h1>
                    <p class="white fs-2 medium">
                        "The Company owns Piling Rig, Excavator, Backhoe-Loader, Motor Grader, Asphalt Paver, Vibratory Roller& other machineries for renting purpose."
                    </p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Construction Equipment Rental</h2> -->
            <p class="fs-3 mt-5">
                The Company introduced construction equipment rental business in 2019 which assists in execution of construction projects smoothly. Not only do we provide construction equipment but also help them meet their respective goals.
            </p>
            <p class="fs-3 mt-5">
                The Company owns Piling Rig, Excavator, Backhoe-Loader, Motor Grader, Asphalt Paver, Vibratory Roller& other machineries for renting purpose.
            </p>
        </div>
    </div>
</div>