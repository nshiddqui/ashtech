<style>
    .bg_overview {
        background: url("img/construction.jpg") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Constructions</h1>
                    <p class="white fs-2 medium">
                        "The Management of the Company is well aware of the future of civil construction & infrastructure business in India."
                    </p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Constructions</h2> -->
            <p class="fs-3 mt-5">
                The Management of the Company is well aware of the future of civil construction & infrastructure business in India. Both civil construction and infrastructure markets offer very significant growth prospects. <b>Started in 2016, it has achieved several milestones until now. The Company has also completed “Construction of Embankment using Fly Ash” for NHAI Road Projects (Eastern Peripheral Expressway - (NH -NE II), Package – III, from KM 46,500 to KM 71,000 in the state of Uttar Pradesh, India for Jaiprakash Associates Limited</b>.
            </p>
            <h2 class="fs-2 bold mt-5">Ongoing Civil Construction Projects:</h2>
            <ul>
                <li class="fs-3 mt-3">
                    Construction of Sadan Bhavan/Nagar Nigam Karyalaya Bhavan, Gorakhpur, Distt.: Gorakhpur(U.P.) Under Jatin Ashtech JV.
                </li>
                <li class="fs-3 mt-3">
                    Construction of 02 Blocks Transit Hostel In Police Line At District Maharajganj Under Ashtech Industries Pvt. Ltd.
                </li>
            </ul>
            <h2 class="fs-2 my-5 bold" style="text-decoration: underline;">Gallery</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 my-2">
                            <a href="/img/construction/IMG-20211231-WA0035.jpg" data-fancybox>
                                <?= $this->Html->image('construction/IMG-20211231-WA0035.jpg', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                            </a>
                        </div>
                        <div class="col-md-6 my-2">
                            <a href="/img/construction/IMG-20211231-WA0031.jpg" data-fancybox>
                                <?= $this->Html->image('construction/IMG-20211231-WA0031.jpg', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                            </a>
                        </div>
                        <div class="col-md-6 my-2">
                            <a href="/img/construction/c-1.png" data-fancybox>
                                <?= $this->Html->image('construction/c-1.png', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                            </a>
                        </div>
                        <div class="col-md-6 my-2">
                            <a href="/img/construction/c-22.png" data-fancybox>
                                <?= $this->Html->image('construction/c-22.png', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 my-2">
                            <a href="/img/construction/img-1.jpeg" data-fancybox>
                                <?= $this->Html->image('construction/img-1.jpeg', ['style' => 'width: 100%; height: calc(172.8px * 2 + 15px); object-fit: cover; object-position: bottom;']) ?>
                            </a>
                        </div>
                        <div class="col-md-6 my-2">
                            <a href="/img/construction/img-2.jpeg" data-fancybox>
                                <?= $this->Html->image('construction/img-2.jpeg', ['style' => 'width: 100%; height: calc(172.8px * 2 + 15px); object-fit: cover; object-position: bottom;']) ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>