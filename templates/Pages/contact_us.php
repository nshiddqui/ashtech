<style>
    .search_container {
        right: 50px;
        top: 10px;
    }

    .radio_bu {
        padding: 9px;
    }

    .Enquiry {
        margin-bottom: 20px;
    }

    div#than_modal .modal-header {
        padding: 8px 10px;
        border: 0px;
    }

    div#than_modal .modal-content {
        background: #C3EFF0;
        color: #000;
        text-align: center;
    }

    div#than_modal .modal-body p {
        font-size: 22px;
        font-weight: 600;
    }

    div#than_modal .modal-header button {
        color: #000 !important;
        opacity: 1;
    }

    .pres_view_web {
        color: #000 !important;
    }

    @media (max-width: 767px) {
        .newformadmin img {
            width: 100%;
        }

        .btn {
            white-space: inherit !important;
        }
    }
</style>
<div class="inner_section">
    <div class="inner_sec">

        <div class="row no-gutters">
            <div class="col-sm-4">
                <div class="cbluebg vunit vh100" style="overflow-x: hidden;">
                    <div class="color active_white" id="regoffcont">
                        <h1 class="fs-2 bold mt-0">Corporate Office</h1>
                        <div class="add_block borderbtm">
                            <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                            <p class="fs-4">
                                C-50, <br />
                                RDC, Raj Nagar
                                <br />
                                Ghaziabad (U.P.), India - 201002.
                            </p>
                            <p class="fs-4">
                                <a href="tel:+919818201927">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/phone.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">+91 012 0453 8000 </span>
                                </a>
                            </p>
                            <p class="fs-4">
                                <a href="mailto:piling@ashtechgroup.in">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/message.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">flyash@ashtechgroup.in</span>
                                </a>
                            </p>
                            <div class="text-center mt-5 locationicon">
                                <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                    <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="color active_white" id="regoffcont">
                        <h1 class="fs-2 bold mt-0">Fly Ash Grinding Unit</h1>
                        <div class="add_block borderbtm">
                            <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                            <p class="fs-4">
                                K-24,<br />
                                MG Road Industrial Area,<br />
                                Hapur, <br />
                                U.P.-245301 INDIA.
                            </p>
                            <p class="fs-4">
                                    <a href="tel:+919818241926">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 98 1824 1926 </span>
                                    </a>
                                </p>
                            <p class="fs-4">
                                <a href="mailto:flyash@ashtechgroup.in">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/message.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">flyash@ashtechgroup.in</span>
                                </a>
                            </p>
                            <div class="text-center mt-5 locationicon">
                                <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                    <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="color active_white" id="regoffcont">
                        <h1 class="fs-2 bold mt-0">Bricks, Blocks & Tiles</h1>
                        <div class="add_block borderbtm">
                            <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                            <p class="fs-4">
                                273,<br />
                                Ranauli Laifpur,<br />
                                Bishara Road, Dadri,<br />
                                Gautam Budh Nagar, UP.
                            </p>
                            <p class="fs-4">
                                <a href="tel:+919810800923">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/phone.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">+91 98 1080 0923 </span>
                                </a>
                            </p>
                            <p class="fs-4">
                                <a href="mailto:bricks@ashtechgroup.in">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/message.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">bricks@ashtechgroup.in</span>
                                </a>
                            </p>
                            <div class="text-center mt-5 locationicon">
                                <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                    <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="color active_white" id="regoffcont">
                        <h1 class="fs-2 bold mt-0">Ready Mix Concrete (Unit-I)</h1>
                        <div class="add_block borderbtm">
                            <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                            <p class="fs-4">
                                Plot No-13A,<br />
                                Udhyog Kendra,<br />
                                Sector - Ecotech-III, Greater Noida,<br />
                                Gautam Budh Nagar, UP 201306
                            </p>
                            <p class="fs-4">
                                <a href="tel:+919871901598">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/phone.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">+91 98 7190 1598 </span>
                                </a>
                            </p>
                            <p class="fs-4">
                                <a href="mailto:rmc@ashtechgroup.in">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/message.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">rmc@ashtechgroup.in</span>
                                </a>
                            </p>
                            <div class="text-center mt-5 locationicon">
                                <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                    <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="color active_white" id="regoffcont">
                        <h1 class="fs-2 bold mt-0">Ready Mix Concrete (Unit-II)</h1>
                        <div class="add_block borderbtm">
                            <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                            <p class="fs-4">
                                233M,<br />
                                Village: Laakhan, Near Jindal Nagar,<br />
                                NH-24,<br />
                                Distt. - Hapur, UP
                            </p>
                            <p class="fs-4">
                                <a href="tel:+919810265614">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/phone.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">+91 98 1026 5614 </span>
                                </a>
                            </p>
                            <p class="fs-4">
                                <a href="tel:+919818404483">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/phone.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">+91 98 1840 4483 </span>
                                </a>
                            </p>
                            <p class="fs-4">
                                <a href="mailto:rmc.jn@ashtechgroup.in">
                                    <span class="d-inline-block align-top mr-2">
                                        <img src="/img/contact/message.png" alt="" /></span>
                                    <span class="d-inline-block align-middle">rmc.jn@ashtechgroup.in</span>
                                </a>
                            </p>
                            <div class="text-center mt-5 locationicon">
                                <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                    <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="color active_white" id="regoffcont">
                        <h1 class="fs-2 bold mt-0">Registered Office</h1>
                        <div class="add_block borderbtm">
                            <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                            <p class="fs-4">
                                D-49,<br />
                                Mansarover Park,<br />
                                Shahdra,<br />
                                Delhi, India - 110032.
                            </p>
                            <div class="text-center mt-5 locationicon">
                                <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                    <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8" style="position:relative">
                <div class="googlemap">
                    <div class="mapanimation register_map zoomactive">
                        <div class="mapouter">
                            <div class="gmap_canvas"><iframe style="height:100vh;width:100%;" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112014.66707522742!2d77.37344235820312!3d28.675932800000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cf1c8ad4ea2ad%3A0xb5c08d4e67edd440!2sAshtech%20Industries%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1634709484363!5m2!1sen!2sin" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de/webdesign-hannover/"></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="contact_mobile">
        <div class="cont_addres_tab">

            <div class="tab-content clearfix cont_tabcontext">
                <div class="tab-pane active" id="reg_office">
                    <div class="add_block cont_mobile_cont">
                        <div class="color active_white" id="regoffcont">
                            <h2 class="fs-2 bold mt-0">Corporate Office</h2>
                            <div class="add_block borderbtm">
                                <h2 class="fs-2a">Ashtech Industries Pvt. Ltd.</h2>
                                <p class="fs-4">
                                    C-50, <br />
                                    RDC, Raj Nagar
                                    <br />
                                    Ghaziabad (U.P.), India - 201002.
                                </p>
                                <p class="fs-4">
                                    <a href="tel:+919818201927">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 012 0453 8000</span>
                                    </a>
                                </p>
                                <p class="fs-4">
                                    <a href="mailto:piling@ashtechgroup.in">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/message.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">flyash@ashtechgroup.in</span>
                                    </a>
                                </p>
                                <div class="text-center mt-5 locationicon">
                                    <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                        <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="color active_white" id="regoffcont">
                            <h1 class="fs-2 bold mt-0">Fly Ash Grinding Unit</h1>
                            <div class="add_block borderbtm">
                                <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                                <p class="fs-4">
                                    K-24,<br />
                                    MG Road Industrial Area,<br />
                                    Hapur, <br />
                                    U.P.-245301 INDIA.
                                </p>
                                <p class="fs-4">
                                    <a href="tel:+919818241926">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 98 1824 1926 </span>
                                    </a>
                                </p>
                                <p class="fs-4">
                                    <a href="mailto:flyash@ashtechgroup.in">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/message.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">flyash@ashtechgroup.in</span>
                                    </a>
                                </p>
                                <div class="text-center mt-5 locationicon">
                                    <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                        <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="color active_white" id="regoffcont">
                            <h1 class="fs-2 bold mt-0">Bricks, Blocks & Tiles</h1>
                            <div class="add_block borderbtm">
                                <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                                <p class="fs-4">
                                    273,<br />
                                    Ranauli Laifpur,<br />
                                    Bishara Road, Dadri,<br />
                                    Gautam Budh Nagar, UP.
                                </p>
                                <p class="fs-4">
                                    <a href="tel:+919810800923">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 98 1080 0923 </span>
                                    </a>
                                </p>
                                <p class="fs-4">
                                    <a href="mailto:bricks@ashtechgroup.in">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/message.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">bricks@ashtechgroup.in</span>
                                    </a>
                                </p>
                                <div class="text-center mt-5 locationicon">
                                    <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                        <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="color active_white" id="regoffcont">
                            <h1 class="fs-2 bold mt-0">Ready Mix Concrete (Unit-I)</h1>
                            <div class="add_block borderbtm">
                                <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                                <p class="fs-4">
                                    Plot No-13A,<br />
                                    Udhyog Kendra,<br />
                                    Sector - Ecotech-III, Greater Noida,<br />
                                    Gautam Budh Nagar, UP 201306
                                </p>
                                <p class="fs-4">
                                    <a href="tel:+919871901598">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 98 7190 1598 </span>
                                    </a>
                                </p>
                                <p class="fs-4">
                                    <a href="mailto:rmc@ashtechgroup.in">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/message.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">rmc@ashtechgroup.in</span>
                                    </a>
                                </p>
                                <div class="text-center mt-5 locationicon">
                                    <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                        <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="color active_white" id="regoffcont">
                            <h1 class="fs-2 bold mt-0">Ready Mix Concrete (Unit-II)</h1>
                            <div class="add_block borderbtm">
                                <h1 class="fs-2a">Ashtech Industries Pvt. Ltd.</h1>
                                <p class="fs-4">
                                    233M,<br />
                                    Village: Laakhan, Near Jindal Nagar,<br />
                                    NH-24,<br />
                                    Distt. - Hapur, UP
                                </p>
                                <p class="fs-4">
                                    <a href="tel:+919810265614">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 98 1026 5614 </span>
                                    </a>
                                </p>
                                <p class="fs-4">
                                    <a href="tel:+919818404483">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/phone.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">+91 98 1840 4483 </span>
                                    </a>
                                </p>
                                <p class="fs-4">
                                    <a href="mailto:rmc.jn@ashtechgroup.in">
                                        <span class="d-inline-block align-top mr-2">
                                            <img src="/img/contact/message.png" alt="" /></span>
                                        <span class="d-inline-block align-middle">rmc.jn@ashtechgroup.in</span>
                                    </a>
                                </p>
                                <div class="text-center mt-5 locationicon">
                                    <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                        <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="color active_white" id="regoffcont">
                            <h2 class="fs-2 bold mt-0">Registered Office</h2>
                            <div class="add_block borderbtm">
                                <h2 class="fs-2a">Ashtech Industries Pvt. Ltd.</h2>
                                <p class="fs-4">
                                    D-49,<br />
                                    Mansarover Park,<br />
                                    Shahdra,<br />
                                    Delhi, India - 110032.
                                </p>
                                <div class="text-center mt-5 locationicon">
                                    <a href="javascript:void(0);" class=" d-inline-block text-uppercase btn orangebtn bold" id="regoffice">
                                        <i class="fa fa-map-marker activelocation" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contact_tab_iframe">
                        <iframe class="contact_iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112014.66707522742!2d77.37344235820312!3d28.675932800000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cf1c8ad4ea2ad%3A0xb5c08d4e67edd440!2sAshtech%20Industries%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1634709484363!5m2!1sen!2sin" frameborder="0" style="border: 0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>