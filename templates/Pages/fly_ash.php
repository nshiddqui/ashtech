<style>
    .bg_overview {
        background: url("img/fly-ash/IMG_7911.JPG") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Fly Ash</h1>
                    <p class="white fs-2 medium">
                       "The Company supplies fly ash to large number of companies since 1993. For high quality fly ash, our Company has an agreement with NTPC Dadri for a period of 20 years."
                    </p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Fly Ash</h2> -->
            <p class="fs-3 mt-5">
                <b>The Company supplies fly ash to large number of companies since 1993</b>. For high quality fly ash, our <b>Company has an agreement with NTPC Dadri for a period of 20 years</b>. It has huge infrastructure to handle fly ash including land & building, silos and other machineries & a fleet of its own vehicles.
            </p>
            <p class="fs-3 mt-5">
                At present, the <b>Company is handling approximately 7.5 Lakh Tonnes of Fly Ash per annum</b>. The Company is procuring fly ash through competitive bids from various thermal power plants on an ongoing basis. Besides, the Company has qualified to procure 0.65 MT fly ash yearly for a period of 15 years from UPRVUNL, Jawaharpur (Etah) Plant.
            </p>
            <h2 class="fs-2 bold">Processed Fly Ash</h2>
            <p class="fs-3 mt-5">
                The <b>Company has a Ball Mill installed at Industrial Area, MG Road, Hapur, Uttar Pradesh for grinding of fly ash</b> as per specific quality requirement of its customers.
            </p>
            <p class="fs-3 mt-5">
                <b>It also has a Drying Unit, Classifying Unit and Ball Mill at Village-Dehra, Hapur, Uttar Pradesh for processing of fly ash</b> to make it quality and superior product for RMC as per requirement of design mix.
            </p>
            <p class="fs-3 mt-5">
                <b>The processed fly ash conforms to IS 3812 standard</b>. The product is supplied in loose carriers as well as packed bags as per orders <b>under “Ashbond” brand name</b>.
            </p>

            <h2 class="fs-2 my-5 bold" style="text-decoration: underline;">Gallery</h2>
            <div class="row">
                <div class="col-md-3">
                    <a href="/img/fly-ash/IMG_7882.JPG" data-fancybox>
                        <?= $this->Html->image('fly-ash/IMG_7882.JPG', ['style' => 'width:100%']) ?>
                    </a>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/fly-ash/IMG_7897.JPG" data-fancybox>
                        <?= $this->Html->image('fly-ash/IMG_7897.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/fly-ash/IMG_7899.JPG" data-fancybox>
                        <?= $this->Html->image('fly-ash/IMG_7899.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/fly-ash/IMG_7894.JPG" data-fancybox>
                        <?= $this->Html->image('fly-ash/IMG_7894.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>