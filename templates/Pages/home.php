<?php
$this->disableAutoLayout();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>ASHTECH INDUSTRIES PRIVATE LIMITED</title>
    <?= $this->Html->css('vendor') ?>
    <?= $this->Html->css('app') ?>
    <?= $this->Html->css('home-demo') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
    <style>
        .menu_Style {
            margin: 1px 0 0
        }
    </style>
</head>

<body>
    <form method="post" action="./" id="form1">
        <header class="header">
            <a class="back_to_top" href="javascript:void(0);"></a>
            <div class="menubackdrop"></div>
            <div class="logo_nav shadow vhidden">
                <a href="/" class="logo_a">
                    <?= $this->Html->image('home/logo.png', ['class' => 'img-fluid']) ?>
                </a>
                <span></span>
                <div class="d-inline-block align-middle"> <button class="hamburger hamburger--spin" type="button" id="trigger-menu"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> <span class="bold d-block menu_Style">MENU</span> </button></div>
            </div>
            <div id="myNav" class="overlay">
                <ul class="nav_one medium text-capitalize">
                    <li><?= $this->Html->link('home', '/') ?></li>
                    <li class="dropdownli">
                        <a href="javascript:void(0)" id="one">about</a>
                        <ul id="onesub" class="submenuul">
                            <li><?= $this->Html->link('Overview', '/overview') ?></li>
                            <li><?= $this->Html->link('Our Story', '/our-story') ?></li>
                            <li><?= $this->Html->link('Leadership Team', '/leadership-team') ?></li>
                            <li><?= $this->Html->link('Values', '/values') ?></li>
                        </ul>
                        <span class="angledown">❯</span>
                    </li>
                    <li class="dropdownli">
                        <a href="javascript:void(0)" id="two">business</a>
                        <ul id="twosub" class="submenuul">
                            <li><?= $this->Html->link('Fly Ash', '/fly-ash') ?></li>
                            <li><?= $this->Html->link('Ready Mix Concrete', '/ready-mix-concrete') ?></li>
                            <li><?= $this->Html->link('Brick Blocks & Tiles', '/brick-blocks-tiles') ?></li>
                            <li><?= $this->Html->link('Constructions', '/constructions') ?></li>
                            <li><?= $this->Html->link('Pile Foundation', '/pile-foundation') ?></li>
                        </ul>
                        <span class="angledown">❯</span>
                    </li>
                    <li><?= $this->Html->link('Clients', '/clients') ?></li>
                    <li><?= $this->Html->link('Awards & Certifications', '/awards') ?></li>
                    <li><?= $this->Html->link('contact us', '/contact-us') ?></li>
                </ul>
                </ul>
            </div>
            <div class="top_links vhidden">
                <ul>
                    <li><a href="javascript:void(0);" class="tl_icons one" onclick="goToSectionHome(2);"></a> <span class="bold">fly ash</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons two" onclick="goToSectionHome(3);"></a> <span class="bold">ready mix concrete</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons three" onclick="goToSectionHome(4);"></a> <span class="bold">Bricks,blocks & tiles</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons four" onclick="goToSectionHome(5);"></a> <span class="bold">Govt. Contractorship</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons seven" onclick="goToSectionHome(6);"></a> <span class="bold">pile foundation</span></li>
                </ul>
            </div>
        </header>
        <div id="gmr-home">
            <div class="section h_sec_home" id="section1">
                <div class="slide active" id="slide12" data-anchor="slide12">
                    <div class="vid_bg vunit vh100 row no-gutters align-items-center text-center">
                        <div class="col-12">
                            <figure><img src="img/home/gmr-logo.png" class="img-fluid" alt="" /></figure>
                            <h1 class="fs-1 my-5">ASHTECH INDUSTRIES PRIVATE LIMITED</h1> <a href="javascript:void(0);" class="play-btn watchvideo plavidinniframe">&nbsp;</a>
                        </div>
                        <div class="vidbox"> <a href="javascript:void(0);" class="closevid1 bold iframeclose"> <img id="close" src="/img/home/error.png" alt="Image of an exit button" /></a> <iframe id="video" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="height: 100%;width: 100%"></iframe></div>
                    </div>
                </div>
            </div>
            <div class="section h_sec_airport" id="section2">
                <div class="row vunit vh100 align-items-center text-center justify-content-center">
                    <div class="col-10">
                        <div class=" airports"><img src="img/ash.jpg" style="height: 100px;border-radius: 5px; width:auto; background-color:white;"></div>
                        <h2 class="fs-2 my-3 bold text-uppercase">fly ash</h2>
                        <div class="row no-gutters justify-content-center">
                            <div class="col-sm-8">
                                <p class="fs-3 medium">
                                    The Company is handling approximately 7.5 Lakh Tonnes of Fly Ash per annum.
                                    The Company supplies <b>Fly Ash</b> to large number of companies since <b>1993</b>.
                                </p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <?= $this->Html->link('more', '/fly-ash', ['class' => 'btn btn-1 bold"']) ?></div>
                    </div>
                </div>
            </div>
            <div class="section h_sec_energy" id="section3">
                <div class="row vunit vh100 align-items-center text-center justify-content-center">
                    <div class="col-10">
                        <div class=" energy"><img src="img/transitmixture.png" style="height: 100px;border-radius: 5px; width:auto; background-color:white;"></div>
                        <h2 class="fs-2 my-3 bold text-uppercase">Ready Mix Concrete</h2>
                        <div class="row no-gutters justify-content-center">
                            <div class="col-sm-8">
                                <p class="fs-3 medium">After the mega success of its previous ventures, the Group engaged itself in another business to manufacture Ready Mix Concrete (RMC).</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <?= $this->Html->link('more', '/ready-mix-concrete', ['class' => 'btn btn-1 bold"']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section h_sec_transport" id="section4">
                <div class="row vunit vh100 align-items-center text-center justify-content-center">
                    <div class="col-10">
                        <div class=" transport"><img src="img/bricks.jpg" style="height: 100px;border-radius: 5px; width:auto; background-color:white;"></div>
                        <h2 class="fs-2 my-3 bold text-uppercase">Brick Blocks Tiles</h2>
                        <div class="row no-gutters justify-content-center">
                            <div class="col-sm-8">
                                <p class="fs-3 medium">With the success of each business vertical, the Company started introducing new verticals under the umbrella of Ashtech Group.</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <?= $this->Html->link('more', '/brick-blocks-tiles', ['class' => 'btn btn-1 bold"']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section h_sec_urban" id="section5">
                <div class="row vunit vh100 align-items-center text-center justify-content-center">
                    <div class="col-10">
                        <div class=" urban"><img src="img/contruction.jpg" style="height: 100px;border-radius: 5px; width:auto; background-color:white;"></div>
                        <h2 class="fs-2 my-3 bold text-uppercase">Govt. Contractorship</h2>
                        <div class="row no-gutters justify-content-center">
                            <div class="col-sm-8">
                                <p class="fs-3 medium">The Management of the Company is well aware of the future of civil construction & infrastructure business in India.</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <?= $this->Html->link('more', '/constructions', ['class' => 'btn btn-1 bold"']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section h_sec_businessServices" id="section6">
                <div class="row vunit vh100 align-items-center text-center justify-content-center">
                    <div class="col-10">
                        <div class=" businessservices"><img src="img/mate180.png" style="height: 100px;border-radius: 5px; width:auto; background-color:white;"></div>
                        <h2 class="fs-2 my-3 bold text-uppercase">Pile Foundation</h2>
                        <div class="row no-gutters justify-content-center">
                            <div class="col-sm-8">
                                <p class="fs-3 medium">AIPL is a glorified high grade Pile foundation work service provider.</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <?= $this->Html->link('more', '/pile-foundation', ['class' => 'btn btn-1 bold"']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pageLoader"></div> <a href="javascript:void(0);" class="closevid bold iframeclose"> <img src="/img/home/error.png" alt="Image of a exit button" /></a>
        <div class="page_indicator">
            <div class="fillit"></div>
        </div>
        <div class="landscapemode">
            <div class="row no-gutters align-items-center vunit vh100">
                <div class="col">
                    <figure><img src="/img/home-mobile/landscape.png" class="img-fluid" alt="Image indicating smartphone to be switched to landscape mode from portrait mode" /></figure>
                    <h1 class="fs-2 white">Please rotate your device</h1>
                    <p class="fs-3a white">Please go back to portrait mode for the best experience.</p>
                </div>
            </div>
        </div>
    </form>
</body>
<?= $this->Html->script('vendor') ?>
<?= $this->Html->script('promise') ?>
<?= $this->Html->script('fetch') ?>
<?= $this->Html->script('app') ?>
<?= $this->Html->script('jquery-ui') ?>
<script>
    $(window).load(function() {
        if (window.location.href.indexOf("home/?id=1#our-journey/slide10") > -1) {
            $.fn.fullpage.moveSlideRight();
        }
    });
</script>
<script type="text/javascript">
    function doLoad() {
        var ArrSearchData = [];
        var searchString = $("#srch_term").val();
        if (searchString.length >= 3) {
            $.ajax({
                url: "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/suggestions/search",
                data: {
                    q: searchString,
                    customconfig: "2f77565f-9662-4711-b6d0-3d0029c6b126"
                },
                type: "GET",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Ocp-Apim-Subscription-Key", "aa1fdad20dad4c448d8b058ad2a68b9d");
                },
                success: function(response) {
                    if (response != "") {
                        var obj = jQuery.parseJSON(response);
                        $.each(obj.suggestionGroups[0].searchSuggestions, function(key, val) {
                            ArrSearchData.push({
                                label: val.displayText.toString()
                            });
                        });
                        $("#srch_term").autocomplete({
                            source: ArrSearchData,
                            scroll: true,
                            selectFirst: true,
                            multiple: false,
                            multipleSeparator: ";",
                            autoFill: false,
                            minLength: 2
                        });
                        console.log(ArrSearchData);
                    } else {
                        $("#srch_term").catcomplete({
                            delay: 0,
                            source: ""
                        });
                    }
                },
            });
        }
    }
</script>
<script>
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
        },
        _renderMenu: function(ul, items) {
            var that = this,
                currentCategory = "";
            $.each(items, function(index, item) {
                var li;
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
                if (item.category) {
                    li.attr("aria-label", item.category + " : " + item.label);
                }
            });
        },
    });
</script>
<?= $this->Html->script('home') ?>
<script>
    $(".plavidinniframe").click(function() {
        $(".vidbox").addClass("active"), (document.getElementById("video").src = "https://www.youtube.com/embed/K9a9SO5JZX8?autoplay=1"), $(".closevid").fadeIn(), $(".logo_nav").animateCss("fadeOutUp", function() {
            $(".logo_nav").addClass("vhidden");
        }), $(".top_links").animateCss("fadeOutUp", function() {
            $(".top_links").addClass("vhidden");
        }), $(".search_container").animateCss("fadeOutUp", function() {
            $(".search_container").addClass("vhidden");
        }), $(".dropd_top").animateCss("fadeOutUp", function() {
            $(".dropd_top").addClass("vhidden");
        }), $(".sidenav").animateCss("fadeOut", function() {
            $(".sidenav").addClass("vhidden");
        });
    }), $(".iframeclose,.closevid").click(function() {
        (document.getElementById("video").src = ""), $(".vidbox").removeClass("active"), $(".iframeclose,.closevid").fadeOut(), $(".logo_nav").removeClass("vhidden").animateCss("fadeInDown"), $(".top_links").removeClass("vhidden").animateCss("fadeInDown"), $(".search_container").removeClass("vhidden").animateCss("fadeInDown"), $(".dropd_top").removeClass("vhidden").animateCss("fadeInDown"), $(".sidenav").removeClass("vhidden").animateCss("fadeIn");
    });
</script>
<script>
    $(document).ready(function() {
        $("#banner1").css("background-image", "url('/img/home/1978.jpg')");
        $("#banner2").css("background-image", "url('/img/home/1996.jpg')");
        $("#banner3").css("background-image", "url('/img/home/2001.jpg')");
        $("#banner4").css("background-image", "url('/img/home/2003.jpg')");
        $("#banner5").css("background-image", "url('/img/home/2006.jpg')");
        $("#banner6").css("background-image", "url('/img/home/2011.jpg')");
        $("#banner7").css("background-image", "url('/img/home/2014.jpg')");
        $("#banner8").css("background-image", "url('/img/home/2016.jpg')");
        $("#banner9").css("background-image", "url('/img/home/2017.jpg')");
        $("#banner11").css("background-image", "url('/img/home/vizha.jpg')");
    });
</script>

</html>
</head>