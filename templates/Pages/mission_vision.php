<style>
    .bg_overview {
        background: url("/img/about/banner-02.jpg") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<!-- <div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Values</h1>
                    <p class="white fs-2 medium">
                        It has its business locations in the state of UP, Delhi, Haryana & Odisha.
                    </p>
                </div> -->
</div>
</div>
</div>
</div> -->
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <?= $this->Html->image('home/logo.png') ?>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <div class="col-12 mt-3">
        <div class="container">
            <h2 class="fs-2 bold">Mission</h2>
            <p class="fs-3 mt-5">
                As a highly customer-oriented organization with a focus on customer satisfaction, our prime motto is <b>“ensure quality- maintain punctuality”</b>. Delivery of quality products and best service is our priority. We are committed to protect the environment and build a healthy and safe creation for the human being.
            </p>
            <h2 class="fs-2 bold">Vision</h2>
            <p class="fs-3 mt-5">
                To be a Company that believes in leadership in supply of fly ash and related products to maintain green environment and to ensure swift and secure structure and envisages growth through diversification in business lines.
            </p>
        </div>
    </div>
</div>