<style>
    .bg_overview {
        background: url("/img/about/our-story.png") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <?= $this->Html->image('home/logo.png') ?>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Our Story</h2> -->
            <p class="fs-3 mt-5">
                <b>Established in the year 1992</b> at Ghaziabad (Uttar Pradesh in India), AIPL has been widely appreciated for dealing in a product known for precise processing, optimum strength, weather resistance and durability assurance. Under the visionary leadership of the Company founders and chairman<b> Late. shri Satish Agarwal, Mr. Sushil Agarwal, Late. shri Praveen Agarwal and Mr. Shiv Kumar Agarwal(M.D)</b>, The Group has already witnessed dynamic growth through setting up several associate industries in various niches. The Group is expanding swiftly and now eyeing in other fields as well.
            </p>
            <p class="fs-3 mt-5">
                Through doing so and attempting for constant expansion, the Ashtech Group fulfills a dream to bring a balanced economic development in the country for its social uplift and to empower the common masses with fast industrialization.
            </p>
        </div>
    </div>
</div>