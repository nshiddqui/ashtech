<style>
    .bg_overview {
        background: url("/img/about/banner-02.jpg") no-repeat center center;
        background-size: cover;
    }

    .jumborton.airports {
        background-image: url("/img/about/image-01.jpg");
        background-repeat: no-repeat;
        background-position: top left;
        background-size: inherit;
    }

    .jumborton.energy {
        background-image: url("/img/about/image-02.jpg");
        background-repeat: no-repeat;
        background-position: center right;
        background-size: inherit;
    }

    .jumborton.transportation {
        background-image: url("/img/about/image-03.jpg");
        background-repeat: no-repeat;
        background-position: top left;
        background-size: inherit;
    }

    .jumborton.urban {
        background-image: url("/img/about/image-04.jpg");
        background-repeat: no-repeat;
        background-position: top right;
        background-size: inherit;
    }

    .jumborton.sports {
        background-image: url("/img/about/image-05.jpg");
        background-repeat: no-repeat;
        background-position: top left;
        background-size: inherit;
    }

    .jumborton.foundation {
        background-image: url("/img/about/image-06.jpg");
        background-repeat: no-repeat;
        background-position: top right;
        background-size: inherit;
    }

    .about_cont {
        padding: 0;
        width: 53%;
        position: absolute;
        left: 40%;
        bottom: 20%;
    }

    .spot_bg {
        background-image: url(/img/about/dr-rao.jpg);
        background-size: cover;
        background-position: center;
        height: 500px;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <?= $this->Html->image('home/logo.png') ?>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">About Us</h2> -->
            <p class="fs-3 mt-5">
                Incorporated under the Ashtech Group, AIPL (Ashtech Industries Private Limited) is a well-established Company engaged in multiple business conglomerates. Some of its successful ventures include supply of fly ash and processed fly ash(under <b>ASHBOND Brand Name</b>), logistics, manufacturing of fly ash bricks & tiles, manufacturing of ready mix concrete(RMC), manufacturing of AAC blocks, Govt. Contractorship (Civil, Electrical, Roads), pile foundation and construction equipment rental amongst others. After attaining grand success in all previous ventures, now the Company is eyeing in the civil construction and infrastructure business as well. With time, it has achieved many milestones and established itself as a pioneer in introducing environment friendly construction activities.
            </p>
            <p class="fs-3 mt-5">
                With proficient management and dedicated employees, AIPL has made prominent space in the market. It works with a zero defect policy, which helps the company win over the hearts of its customers and partners.
            </p>
            <p class="fs-3">Ashtech is an ISO 9001:2015, ISO14001:2015 & ISO 45001:2018 certified Company.</p>
            <p class="fs-3 mt-5"><?= $this->Html->image('home/logo.png', ['style' => 'height:24px;margin-top:-7px']) ?> is the registered trade mark of Ashtech Group.</p>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <div class="row" style="justify-content: center;">
                <div class="col-md-2 my-2">
                    <div class="card card-1">
                        <?= $this->Html->image('ash.jpg', ['style' => 'width: 100%; height: 150px;', 'url' => '/fly-ash']) ?>
                    </div>
                </div>
                <div class="col-md-2 my-2">
                    <div class="card card-1">
                        <?= $this->Html->image('transitmixture.png', ['style' => 'width: auto; height: 150px;', 'url' => '/ready-mix-concrete']) ?>
                    </div>
                </div>
                <div class="col-md-2 my-2">
                    <div class="card card-1">
                        <?= $this->Html->image('bricks.jpg', ['style' => 'width: auto; height: 150px;', 'url' => '/brick-blocks-tiles']) ?>
                    </div>
                </div>
                <div class="col-md-2 my-2">
                    <div class="card card-1">
                        <?= $this->Html->image('contruction.jpg', ['style' => 'width: auto; height: 150px;', 'url' => '/constructions']) ?>
                    </div>
                </div>
                <div class="col-md-2 my-2">
                    <div class="card card-1">
                        <?= $this->Html->image('mate180.png', ['style' => 'width: auto; height: 150px;', 'url' => '/pile-foundation']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>