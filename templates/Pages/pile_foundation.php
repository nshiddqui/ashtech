<style>
    .bg_overview {
        background: url("img/piling/IMG-20190419-WA0006.jpg") no-repeat center center;
        background-size: cover;

    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Pile Foundation</h1>
                    <p class="white fs-2 medium">
                       "AIPL is a glorified high grade Pile foundation work service provider."
                    </p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Pile Foundation</h2> -->
            <p class="fs-3 mt-5">
                AIPL is a glorified high grade <b>Pile foundation work service provider</b>. Piling work belongs to complex construction areas where precision speaks. Foundation is the most important section of the whole construction and if the laid foundation is weak the structure won’t last long. Piling is a technique that helps in laying strong foundation.
            </p>
            <p class="fs-3 mt-5">
                By the strong foundation we means the structural support that efficiently transfers load to the soil without leading it to collapse. Piling is not just about digging and concreting, rather there are multiple conditions that have their effect, thus, need to be studied before starting the job.
            </p>
            <p class="fs-3 mt-5">
                Being piling service provider first we analyse A to Z estimate and then study working environment deeply. This includes soil conditions, water level, noise & vibration sensitivity, and many more. There are several research and analysis that are to be done before piling foundation.
            </p>
            <p class="fs-3 mt-5">
                We are the renowned constructor because we take our work very seriously. Talking about strategies and technology, we go very picky on them as we do not want to incorporate any low-grade services. We examine soil quality and accordingly process the foundation work. We hear client needs and give our suggestions so that greater results are achieved. The beauty of the piling services is that they are need orientated i.e. laid based on function & material. This means they are completely dedicated to the job they are constructed for. The efficiency of the construction depends upon the primary examination.
            </p>
            <p class="fs-3 mt-5">
                From workers to the supervisors every member of the AIPL (Ashtech Pile Foundation Division) is an expert of his own working area. We adopt latest technology to offer best-in-class Industrial/Infrastructure Piling work. Quality job and safety of our workers are two prominent areas of our focus. Precision is what you’ll get with our collaboration.
            </p>
            <h2 class="fs-2 my-5 bold" style="text-decoration: underline;">Gallery</h2>
            <div class="row">
                <div class="col-md-3">
                    <a href="/img/piling/6-0.jpg" data-fancybox>
                        <?= $this->Html->image('piling/6-0.jpg', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/piling/piling-3.png" data-fancybox>
                        <?= $this->Html->image('piling/piling-3.png', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/piling/piling-1.png" data-fancybox>
                        <?= $this->Html->image('piling/piling-1.png', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/piling/piling-2.png" data-fancybox>
                        <?= $this->Html->image('piling/piling-2.png', ['style' => 'width: 100%; height: 172.8px; object-fit: cover; object-position: bottom;']) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>