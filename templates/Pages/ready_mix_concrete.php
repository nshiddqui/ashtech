<style>
    .bg_overview {
        background: url("img/rmc/IMG_7841.JPG") no-repeat center center;
        background-size: cover;
    }

    .spot_text h4,
    .spot_text p {
        color: #fff;
    }

    .mob_show {
        display: none;
    }

    /*breakpoints*/
    @media only screen and (max-width: 767px) {
        .spot_bg {
            background: none;
            height: auto;
        }

        .spot_text,
        .spot_mobimg {
            margin-top: 20px;
        }

        .spot_text h4,
        .spot_text p {
            color: #000;
        }

        .about_cont {
            position: absolute;
            bottom: -15px;
            left: 4%;
            width: 98%;
        }

        .mob_padd {
            padding: 18px;
        }

        .mob_show {
            display: block;
        }

        .f-14 {
            font-size: 14px;
        }
    }
</style>
<div class="sec_in_home bg_overview">
    <div class="container">
        <div class="row no-gutters align-items-center vunit vh100 ">
            <div class="col-8">
                <!-- <div class="op-bg-blck scrollme animateme" data-when="enter" data-from="0" data-to="1" data-opacity="0.1">
                    <h1 class="white fs-0 bold">Ready Mix Concrete</h1>
                    <p class="white fs-2 medium">
                        "The Company owns all required infra, equipment & Transit Mixers with lab at all plants."
                    </p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="inner_sec bg_cyan row no-gutters sec_py intro_content">
    <div class="col-12">
        <div class="container">
            <!-- <h2 class="fs-2 bold">Ready Mix Concrete</h2> -->
            <p class="fs-3 mt-5">
                After the mega success of its previous ventures, the <b>Group engaged itself in another business to manufacture Ready Mix Concrete (RMC)</b>. The Company started unit with a single Plant (capacity: 60 cum per hour) at <b>Greater Noida, Uttar Pradesh</b> in 2006. In view of enhanced demand of RMC and best in the class services, the management further added 2 more plants (capacity: 60 & 70 cum/hr) at the same location thereafter.
            </p>
            <p class="fs-3 mt-5">
                With the success of the venture, the Company has completed several orders across the industries. It has started its 2nd Unit in 2011 at <b>Jindal Nagar, Hapur, Uttar Pradesh</b> having the capacity of 70 cum/hr.
            </p>
            <p class="fs-3 mt-5">
                Now the <b>total production capacity of both the Units is 260 cum/hr</b>. The Company plans to add more capacity in the coming days.
            </p>
            <p class="fs-3 mt-5">
                The Company owns all required infra, equipment & Transit Mixers with lab at all plants.
            </p>
            <h2 class="fs-2 my-5 bold" style="text-decoration: underline;">Gallery</h2>
            <div class="row">
                <div class="col-md-3">
                    <a href="/img/rmc/IMG_7826.JPG" data-fancybox>
                        <?= $this->Html->image('rmc/IMG_7826.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/rmc/IMG_7830.JPG" data-fancybox>
                        <?= $this->Html->image('rmc/IMG_7830.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/rmc/IMG_7832.JPG" data-fancybox>
                        <?= $this->Html->image('rmc/IMG_7832.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="/img/rmc/IMG_7836.JPG" data-fancybox>
                        <?= $this->Html->image('rmc/IMG_7836.JPG', ['style' => 'width:100%']) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>