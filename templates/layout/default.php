<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>ASHTECH INDUSTRIES PRIVATE LIMITED</title>
    <?= $this->Html->css('vendor') ?>
    <?= $this->Html->css('app') ?>
    <?= $this->Html->css('home-demo') ?>
    <?= $this->Html->css('inner') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
    <?= $this->Html->css('https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css') ?>
    <?= $this->Html->script('https://code.jquery.com/jquery-3.6.0.min.js') ?>
    <?= $this->Html->script('https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js') ?>
    <?= $this->Html->script('vendor') ?>
    <?= $this->Html->script('promise') ?>
    <?= $this->Html->script('fetch') ?>
    <?= $this->Html->script('jquery-ui') ?>
    <style>
        .menu_Style {
            margin: 1px 0 0;
        }
    </style>
    <script>
        Fancybox.bind("[data-fancybox]", {
            // Your options go here
        });
    </script>
</head>

<body>
    <form method="post" action="./" id="form1">
        <header class="header">
            <a class="back_to_top" href="javascript:void(0);"></a>
            <div class="menubackdrop"></div>
            <div class="logo_nav shadow vhidden">
                <a href="/" class="logo_a">
                    <?= $this->Html->image('home/logo.png', ['class' => 'img-fluid']) ?>
                </a>
                <span></span>
                <div class="d-inline-block align-middle"> <button class="hamburger hamburger--spin" type="button" id="trigger-menu"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> <span class="bold d-block menu_Style">MENU</span> </button></div>
            </div>
            <div id="myNav" class="overlay">
                <ul class="nav_one medium text-capitalize">
                    <li><?= $this->Html->link('home', '/') ?></li>
                    <li class="dropdownli">
                        <a href="javascript:void(0)" id="one">about</a>
                        <ul id="onesub" class="submenuul">
                            <li><?= $this->Html->link('Overview', '/overview') ?></li>
                            <li><?= $this->Html->link('Our Story', '/our-story') ?></li>
                            <li><?= $this->Html->link('Leadership Team', '/leadership-team') ?></li>
                            <li><?= $this->Html->link('Mission & Vision', '/mission-vision') ?></li>
                        </ul>
                        <span class="angledown">❯</span>
                    </li>
                    <li class="dropdownli">
                        <a href="javascript:void(0)" id="two">business</a>
                        <ul id="twosub" class="submenuul">
                            <li><?= $this->Html->link('Fly Ash', '/fly-ash') ?></li>
                            <li><?= $this->Html->link('Ready Mix Concrete', '/ready-mix-concrete') ?></li>
                            <li><?= $this->Html->link('Brick Blocks & Tiles', '/brick-blocks-tiles') ?></li>
                            <li><?= $this->Html->link('Govt. Contractorship', '/constructions') ?></li>
                            <li><?= $this->Html->link('Pile Foundation', '/pile-foundation') ?></li>
                        </ul>
                        <span class="angledown">❯</span>
                    </li>
                    <li><?= $this->Html->link('Clients', '/clients') ?></li>
                    <li><?= $this->Html->link('Awards & Certifications', '/awards') ?></li>
                    <li><?= $this->Html->link('contact us', '/contact-us') ?></li>
                </ul>
                </ul>
            </div>
            <div class="top_links vhidden">
                <ul>
                    <li><a href="javascript:void(0);" class="tl_icons one" onclick="goToSectionHome(2);"></a> <span class="bold">fli ash</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons two" onclick="goToSectionHome(3);"></a> <span class="bold">ready mix & tiles</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons three" onclick="goToSectionHome(4);"></a> <span class="bold">brick blocks & tiles</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons four" onclick="goToSectionHome(5);"></a> <span class="bold">constructions</span></li>
                    <li><a href="javascript:void(0);" class="tl_icons seven" onclick="goToSectionHome(6);"></a> <span class="bold">pile foundation</span></li>
                </ul>
            </div>
        </header>
        <?= $this->fetch('content') ?>
        <div class="footer row no-gutters align-items-center justify-content-center text-left">
            <div class="col">
                <div class="container">
                    <span>&copy; 2021 Copyright Ashtech. All Rights Reserved</span>
                    <div class="w-100"></div>
                </div>
            </div>
        </div>
    </form>
    <?= $this->Html->script('inner') ?>
    <?= $this->Html->script('app') ?>
</body>

</html>
</head>