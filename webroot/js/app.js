var deferredPrompt;
function removeSubs() {
    $("#onesub").removeClass("active"), $("#twosub").removeClass("active");
}
function removeSubsBig() {
    $("#onesubbig").removeClass("active");
}
function searchTerm() {
    var e = $("#srch_term").val();
    if ("" == e) return alert("Please enter search term"), $("#srch_term").focus(), !1;
    if ("undefined" == e) return alert("Invalid keyword"), $("#srch_term").val(""), $("#srch_term").focus(), $(".btn_voice").removeClass("searched"), !1;
    var i = e.replace(" ", "-");
    parent.location.href = "/search.aspx?search_query=" + i;
}
window.Promise || (window.Promise = Promise),
    "serviceWorker" in navigator &&
        navigator.serviceWorker
            .register("../sw.js")
            .then(function () {
                console.log("Service worker registered!");
            })
            .catch(function (e) {
                console.log(e);
            }),
    new vUnit({
        CSSMap: {
            ".vh": { property: "height", reference: "vh" },
            ".vw": { property: "width", reference: "vw" },
            ".vwfs": { property: "font-size", reference: "vw" },
            ".vhmt": { property: "margin-top", reference: "vh" },
            ".vhmb": { property: "margin-bottom", reference: "vh" },
            ".vminw": { property: "width", reference: "vmin" },
            ".vmaxw": { property: "width", reference: "vmax" },
        },
    }).init(),
    $(".search_btn").click(function () {
        $("#btn_searchqry").show(),
            $("#srch_term").focus(),
            $(this).hide(),
            $(".btn_voice").show(),
            $(".logo_nav").toggleClass("zindex"),
            $(this).toggleClass("shadow"),
            $(".search_container").toggleClass("active"),
            $(".menubackdrop").fadeToggle();
    }),
    $("#trigger-menu").click(function () {
        $(this).toggleClass("is-active"), $("#myNav").toggleClass("active"), $(".menubackdrop").fadeToggle();
    }),
    $(".menubackdrop").click(function () {
        $("#trigger-menu").removeClass("is-active"),
            $("#myNav").removeClass("active"),
            $(".menubackdrop").fadeOut(),
            $("#btn_searchqry").hide(),
            $(".search_btn").show(),
            $(".btn_voice").hide(),
            $(".search_btn").addClass("shadow"),
            $(".search_container").removeClass("active"),
            $(".logo_nav").removeClass("zindex");
    }),
    $("#basic").selectpicker(),
    $("#basicAirports").selectpicker(),
    $("#basicAbout").selectpicker(),
    $("#basicCareers").selectpicker(),
    $("#basicEnergy").selectpicker(),
    $("#basicUrban").selectpicker(),
    $("#basicServices").selectpicker(),
    $("basicTransportation").selectpicker(),
    $("basicSports").selectpicker(),
    $("#basic").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../about/"),
            1 == i && (window.location = "../airports/"),
            2 == i && (window.location = "../energy/"),
            3 == i && (window.location = "../transportation/"),
            4 == i && (window.location = "../urban/"),
            5 == i && (window.location = "../sports/"),
            6 == i && (window.location = "../other-businesses/"),
            7 == i && (window.location = "../foundation/");
    }),
    $("#basicAirports").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../airports/"),
            2 == i && (window.location = "../delhi-international-airport/"),
            3 == i && (window.location = "../hyderabad-international-airport/"),
			4 == i && (window.location = "../bidar/"),
            5 == i && (window.location = "../cebu/"),
            7 == i && (window.location = "../goa/"),
            8 == i && (window.location = "../vishakapatnam/"),
            9 == i && (window.location = "../greece/");
			10 == i && (window.location = "../airport-land-development/");
    }),
    $("#basicServices").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../servicesbusiness/"),
            1 == i && (window.location = "../academy/"),
            2 == i && (window.location = "../aviation/"),
            3 == i && (window.location = "../raxa-tech/"),
            4 == i && (window.location = "../GEMS/");
    }),
    $("#basicAbout").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../about/"),
            1 == i && (window.location = "../vision-values-beliefs/"),
            2 == i && (window.location = "../corporate-governance/"),
            3 == i && (window.location = "../group-holdings/"),
            4 == i && (window.location = "../our-partners/"),
            5 == i && (window.location = "../our-journey/"),
            6 == i && (window.location = "../our-presence/"),
            7 == i && (window.location = "../awards/");
    }),
    $("#basicCareers").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../careers/"),
            1 == i && (window.location = "../working-at-gmr/"),
            2 == i && (window.location = "../faq/"),
            3 == i && window.open("https://careers.gmrgroup.in/", "_blank"),
            4 == i && (window.location = "../fake-job/");
    }),
    window.location.href.indexOf("servicesbusiness") > -1 && $("#basicServices").selectpicker("val", "0"),
    window.location.href.indexOf("academy") > -1 && $("#basicServices").selectpicker("val", "1"),
    window.location.href.indexOf("aviation") > -1 && $("#basicServices").selectpicker("val", "2"),
    window.location.href.indexOf("raxa-tech") > -1 && $("#basicServices").selectpicker("val", "3"),
    window.location.href.indexOf("GEMS") > -1 && $("#basicServices").selectpicker("val", "4"),
    window.location.href.indexOf("energy") > -1 && $("#basic").selectpicker("val", "3"),
    window.location.href.indexOf("transportation") > -1 && $("#basic").selectpicker("val", "4"),
    window.location.href.indexOf("urban") > -1 && $("#basic").selectpicker("val", "5"),
    window.location.href.indexOf("sports") > -1 && $("#basic").selectpicker("val", "6"),
    window.location.href.indexOf("other-businesses") > -1 && $("#basic").selectpicker("val", "7"),
    window.location.href.indexOf("foundation") > -1 && $("#basic").selectpicker("val", "8"),
    window.location.href.indexOf("careers") > -1 && $("#basicCareers").selectpicker("val", "1"),
    window.location.href.indexOf("working-at-gmr") > -1 && $("#basicCareers").selectpicker("val", "2"),
    window.location.href.indexOf("faq") > -1 && $("#basicCareers").selectpicker("val", "3"),
    window.location.href.indexOf("fake-job") > -1 && $("#basicCareers").selectpicker("val", "5"),
    window.location.href.indexOf("airports") > -1 && $("#basicAirports").selectpicker("val", "0"),
    window.location.href.indexOf("delhi-international-airport") > -1 && $("#basicAirports").selectpicker("val", "2"),
    window.location.href.indexOf("hyderabad-international-airport") > -1 && $("#basicAirports").selectpicker("val", "3"),
    window.location.href.indexOf("bidar") > -1 && $("#basicAirports").selectpicker("val", "4"),
	window.location.href.indexOf("cebu") > -1 && $("#basicAirports").selectpicker("val", "5"),
    window.location.href.indexOf("goa") > -1 && $("#basicAirports").selectpicker("val", "7"),
    window.location.href.indexOf("vishakapatnam") > -1 && $("#basicAirports").selectpicker("val", "8"),
    window.location.href.indexOf("greece") > -1 && $("#basicAirports").selectpicker("val", "9"),
	window.location.href.indexOf("airport-land-development") > -1 && $("#basicAirports").selectpicker("val", "10"),
    window.location.href.indexOf("about") > -1 && $("#basicAbout").selectpicker("val", "1"),
    window.location.href.indexOf("vision-values-beliefs") > -1 && $("#basicAbout").selectpicker("val", "2"),
    window.location.href.indexOf("corporate-governance") > -1 && $("#basicAbout").selectpicker("val", "3"),
    window.location.href.indexOf("group-holdings") > -1 && $("#basicAbout").selectpicker("val", "4"),
    window.location.href.indexOf("our-partners") > -1 && $("#basicAbout").selectpicker("val", "5"),
    window.location.href.indexOf("our-journey") > -1 && $("#basicAbout").selectpicker("val", "6"),
    window.location.href.indexOf("our-presence") > -1 && $("#basicAbout").selectpicker("val", "7"),
    window.location.href.indexOf("awards") > -1 && $("#basicAbout").selectpicker("val", "8"),
    $("#basicSports").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../sports/"), 1 == i && window.open("https://www.delhidaredevils.com/", "_blank"), 2 == i && window.open("https://www.prokabaddi.com/teams/30-up-yoddha-teamprofile", "_blank");
    }),
    window.location.href.indexOf("sports") > -1 && $("#basicSports").selectpicker("val", "0"),
    window.location.href.indexOf("https://www.delhidaredevils.com/") > -1 && $("#basicSports").selectpicker("val", "1"),
    window.location.href.indexOf("https://www.prokabaddi.com/teams/30-up-yoddha-teamprofil") > -1 && $("#basicSports").selectpicker("val", "2"),
    $("#basicOtherBus").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../other-businesses/"),
            1 == i && window.open("https://www.gmraerospacepark.in/", "_blank"),
            2 == i && window.open("https://www.gmrarena.com/", "_blank"),
            3 == i && window.open("https://gmrinnovex.com/", "_blank"),
            4 == i && window.open("https://gmraviationacademy.org/", "_blank"),
            5 == i && window.open("http://www.raxatechnosecuritysolutions.in/", "_blank");
    }),
    window.location.href.indexOf("other-businesses") > -1 && $("#basicOtherBus").selectpicker("val", "0"),
    window.location.href.indexOf("https://www.gmraerospacepark.in/") > -1 && $("#basicOtherBus").selectpicker("val", "1"),
    window.location.href.indexOf("https://www.gmrarena.com/") > -1 && $("#basicOtherBus").selectpicker("val", "2"),
    window.location.href.indexOf("https://gmrinnovex.com/") > -1 && $("#basicOtherBus").selectpicker("val", "3"),
    window.location.href.indexOf("https://gmraviationacademy.org/") > -1 && $("#basicOtherBus").selectpicker("val", "4"),
    window.location.href.indexOf("http://www.raxatechnosecuritysolutions.in/") > -1 && $("#basicOtherBus").selectpicker("val", "5"),
    $(".other_businesses_slick").slick({
        centerMode: !0,
        centerPadding: "0px",
        slidesToShow: 1,
        responsive: [
            { breakpoint: 768, settings: { arrows: !0, centerMode: !0, centerPadding: "0px", slidesToShow: 1 } },
            { breakpoint: 480, settings: { arrows: !0, centerMode: !0, centerPadding: "0px", slidesToShow: 1 } },
        ],
    }),
    $(".logoslick").slick({
        centerMode: !0,
        centerPadding: "0px",
        slidesToShow: 1,
        responsive: [
            { breakpoint: 768, settings: { arrows: !0, centerMode: !0, centerPadding: "0px", slidesToShow: 1 } },
            { breakpoint: 480, settings: { arrows: !0, centerMode: !0, centerPadding: "0px", slidesToShow: 1 } },
        ],
    }),
    $(".dropdownli>a#one").click(function () {
        removeSubsBig(), $("#twosub").removeClass("active"), $("#onesub").toggleClass("active");
    }),
    $(".dropdownli>a#two").click(function () {
        removeSubsBig(), $("#onesub").removeClass("active"), $("#twosub").toggleClass("active");
    }),
    $(".dropdownli>a#onebig").click(function () {
        removeSubs(), $("#onesubbig").toggleClass("active");
    }),
    $("#myNav").enscroll({ showOnHover: !0, verticalTrackClass: "track3", verticalHandleClass: "handle3" }),
    $(".blue-tab").click(function () {
        $(".blue-tab").toggleClass("active"), $(".orange-tab").removeClass("active"), $(".groupsites").slideUp(), $(".sitemaplink").slideToggle(), $("html, body").animate({ scrollTop: $(".trgbtn").offset().top }, 1e3);
    }),
    $(".orange-tab").click(function () {
        $(".orange-tab").toggleClass("active"), $(".blue-tab").removeClass("active"), $(".sitemaplink").slideUp(), $(".groupsites").slideToggle(), $("html, body").animate({ scrollTop: $(".trgbtn").offset().top }, 1e3);
    }),
    $("#basicEnergy").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../energy/"),
            1 == i && (window.location = "../gmr-tenaga/"),
            3 == i && (window.location = "../gel-kakinada/"),
            4 == i && (window.location = "../vemagiri/"),
            5 == i && (window.location = "../warora-energy-ltd/"),
            6 == i && (window.location = "../kamalanga/"),
            7 == i && (window.location = "../windplant-tamilnadu/"),
            8 == i && (window.location = "../windplant-gujarat/"),
            9 == i && (window.location = "../solar-gujarat/"),
            11 == i && (window.location = "../bajoli-project/"),
            12 == i && (window.location = "../sjk-powergen/"),
            13 == i && (window.location = "../upper-karnali/"),
            14 == i && (window.location = "../himtal-hydropower/"),
            15 == i && (window.location = "../badrinath-hydro/"),
            16 == i && (window.location = "../talong-power/"),
            17 == i && (window.location = "../power-transmission/"),
            19 == i && (window.location = "../coal-international/"),
            20 == i && (window.location = "../power-trading/");
    }),
    window.location.href.indexOf("energy") > -1 && $("#basicEnergy").selectpicker("val", "0"),
    window.location.href.indexOf("gmr-tenaga") > -1 && $("#basicEnergy").selectpicker("val", "1"),
    window.location.href.indexOf("gel-kakinada") > -1 && $("#basicEnergy").selectpicker("val", "3"),
    window.location.href.indexOf("vemagiri") > -1 && $("#basicEnergy").selectpicker("val", "4"),
    window.location.href.indexOf("warora-energy-ltd") > -1 && $("#basicEnergy").selectpicker("val", "5"),
    window.location.href.indexOf("kamalanga") > -1 && $("#basicEnergy").selectpicker("val", "6"),
    window.location.href.indexOf("windplant-tamilnadu") > -1 && $("#basicEnergy").selectpicker("val", "7"),
    window.location.href.indexOf("windplant-gujarat") > -1 && $("#basicEnergy").selectpicker("val", "8"),
    window.location.href.indexOf("solar-gujarat") > -1 && $("#basicEnergy").selectpicker("val", "9"),
    window.location.href.indexOf("bajoli-project") > -1 && $("#basicEnergy").selectpicker("val", "11"),
    window.location.href.indexOf("sjk-powergen") > -1 && $("#basicEnergy").selectpicker("val", "12"),
    window.location.href.indexOf("upper-karnali") > -1 && $("#basicEnergy").selectpicker("val", "13"),
    window.location.href.indexOf("himtal-hydropower") > -1 && $("#basicEnergy").selectpicker("val", "14"),
    window.location.href.indexOf("badrinath-hydro") > -1 && $("#basicEnergy").selectpicker("val", "15"),
    window.location.href.indexOf("talong-power") > -1 && $("#basicEnergy").selectpicker("val", "16"),
    window.location.href.indexOf("power-transmission") > -1 && $("#basicEnergy").selectpicker("val", "17"),
    window.location.href.indexOf("coal-international") > -1 && $("#basicEnergy").selectpicker("val", "19"),
    window.location.href.indexOf("power-trading") > -1 && $("#basicEnergy").selectpicker("val", "20"),
    $("#basicTransportaion").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../transportation/"),
            2 == i && (window.location = "../adloor-gundla-pochannpalli/"),
            3 == i && (window.location = "../chennai-outer-ring-road/"),
            4 == i && (window.location = "../ambala-chandigarh/"),
            5 == i && (window.location = "../hyderabad-vijaywada/"),
            6 == i && (window.location = "../epc-division/");
    }),
    window.location.href.indexOf("transportation") > -1 && $("#basicTransportaion").selectpicker("val", "0"),
    window.location.href.indexOf("adloor-gundla-pochannpalli") > -1 && $("#basicTransportaion").selectpicker("val", "2"),
    window.location.href.indexOf("chennai-outer-ring-road") > -1 && $("#basicTransportaion").selectpicker("val", "3"),
    window.location.href.indexOf("ambala-chandigarh") > -1 && $("#basicTransportaion").selectpicker("val", "4"),
    window.location.href.indexOf("hyderabad-vijaywada") > -1 && $("#basicTransportaion").selectpicker("val", "5"),
    window.location.href.indexOf("epc-division") > -1 && $("#basicTransportaion").selectpicker("val", "6"),
    window.location.href.indexOf("urban") > -1 && $("#basicUrban").selectpicker("val", "0"),
    window.location.href.indexOf("krishnagiri-sir-tamilnadu") > -1 && $("#basicUrban").selectpicker("val", "1"),
    $("#basicUrban").on("changed.bs.select", function (e, i, o, n) {
        0 == i && (window.location = "../urban/"), 1 == i && (window.location = "../krishnagiri-sir-tamilnadu/");
    }),
    $("#srch_term").bind("keypress", function (e) {
        13 == (e.keyCode ? e.keyCode : e.which) && (e.preventDefault(), searchTerm());
    });
