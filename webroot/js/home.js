function goToSectionHome(e) {
    $.fn.fullpage.moveTo(e);
}

function applyOrientation() {
    window.innerHeight > window.innerWidth ? ($.fn.fullpage.reBuild(), $(".landscapemode").removeClass("active")) : $(".landscapemode").addClass("active");
}

function chairmanbg() {
    $(".h_sec_about.h_sec_aboutdemo").css("background", 'url("../src/images/home/new3/home board message bg.jpg")');
}

function sportbg() {
    $(".h_sec_about.h_sec_aboutdemo").css("background", 'url("../src/images/home/new3/home sportlight bg.jpg")');
}

function visionbg() {
    $(".h_sec_about.h_sec_aboutdemo").css("background", 'url("../src/images/home/new3/home vision statment bg.jpg")');
}

function investbg() {
    $(".h_sec_about.h_sec_aboutdemo").css("background", 'url("../src/images/home/new3/home investor bg.jpg")');
}
window.matchMedia("(max-width: 767px)").matches && $(".h_sec_about").addClass("fp-auto-height"),
    $(document).ready(function() {
        $("#gmr-home").fullpage({
                navigation: !1,
                anchors: ["our-journey", "fly-ash", "ready-mix-concrete", "brick-blocks-tiles", "constructions", "pile-foundation"],
                slidesNavigation: !0,
                slidesNavPosition: "bottom",
                loopHorizontal: !0,
                scrollOverflow: !0,
                scrollOverflowOptions: { scrollbars: !0, mouseWheel: !0, hideScrollbars: !1, fadeScrollbars: !1, disableMouse: !0 },
                onLeave: function(e, s, a) {
                    console.log(s);
                    1 != s && $(".back_to_top").fadeIn(),
                        1 == s && $(".back_to_top").fadeOut(),
                        1 == s && $(".fillit").css("width", "11.1111111111%"),
                        $(".tl_icons").removeClass("active"),
                        2 == s &&
                        ($(".fillit").css("width", "33.3333333333%"),
                            $("#section2 .head_icon").animateCss("fadeInUp"),
                            $("#section2 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.one").addClass("active")),
                        3 == s &&
                        ($(".fillit").css("width", "44.4444444444%"),
                            $("#section3 .head_icon").animateCss("fadeInUp"),
                            $("#section3 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.two").addClass("active")),
                        4 == s &&
                        ($(".fillit").css("width", "55.5555555556%"),
                            $("#section4 .head_icon").animateCss("fadeInUp"),
                            $("#section4 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.three").addClass("active")),
                        5 == s &&
                        ($(".fillit").css("width", "66.6666666667%"),
                            $("#section5 .head_icon").animateCss("fadeInUp"),
                            $("#section5 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.four").addClass("active")),
                        6 == s &&
                        ($(".fillit").css("width", "77.7777777778%"),
                            $("#section6 .head_icon").animateCss("fadeInUp"),
                            $("#section6 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.seven").addClass("active")),
                        7 == s &&
                        ($(".fillit").css("width", "88.7777777778%"),
                            $("#section7 .head_icon").animateCss("fadeInUp"),
                            $("#section7 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.six").addClass("active")),
                        8 == s &&
                        ($(".fillit").css("width", "88.8888888889%"),
                            $("#section8 .head_icon").animateCss("fadeInUp"),
                            $("#section8 .btn").animateCss("fadeInUp"),
                            $(".tl_icons").removeClass("active"),
                            $(".tl_icons.eight").addClass("active"),
                            setTimeout(function() {
                                $(".business_icons.one").addClass("active");
                            }, 900),
                            setTimeout(function() {
                                $(".business_icons.one").removeClass("active"), $(".business_icons.two").addClass("active");
                            }, 1800),
                            setTimeout(function() {
                                $(".business_icons.two").removeClass("active"), $(".business_icons.three").addClass("active");
                            }, 2700),
                            setTimeout(function() {
                                $(".business_icons.three").removeClass("active"), $(".business_icons.four").addClass("active");
                            }, 3600),
                            setTimeout(function() {
                                $(".business_icons.four").removeClass("active");
                            }, 4500)),
                        8 == s && ($(".fillit").css("width", "100%"), $("#section8 .head_icon").animateCss("fadeInUp"), $("#section8 .btn").animateCss("fadeInUp"), $(".tl_icons").removeClass("active"), $(".tl_icons.six").addClass("active"));
                    9 == s && ($(".fillit").css("width", "100%"), $("#section9 .head_icon").animateCss("fadeInUp"), $("#section9 .btn").animateCss("fadeInUp"), $(".tl_icons").removeClass("active"), $(".tl_icons.seven").addClass("active"));
                },
                afterLoad: function(e, s) {},
                afterRender: function() {
                    console.log("Rendered"),
                        setTimeout(function() {
                            $(".pageLoader").animateCss("fadeOutRight", function() {
                                $(".pageLoader").hide(),
                                    $(".logo_nav")
                                    .removeClass("vhidden")
                                    .animateCss("fadeInLeft", function() {
                                        $(".top_links")
                                            .removeClass("vhidden")
                                            .animateCss("fadeInDown", function() {
                                                $(".search_container").removeClass("vhidden").animateCss("fadeInRight"), $(".dropd_top").removeClass("vhidden").animateCss("fadeInRight");
                                            });
                                    });
                            });
                        }, 900),
                        $(".fp-prev").addClass("hideImp"),
                        $(".fp-next").addClass("hideImp"),
                        setInterval(function() {
                            $(".getdown").animateCss("slideInDown");
                        }, 1e3),
                        $(".our-journey").click(function() {
                            $.fn.fullpage.moveSlideRight();
                        }),
                        $(".getdown").click(function() {
                            $.fn.fullpage.moveSectionDown();
                        });
                },
                afterResize: function() {},
                afterSlideLoad: function(e, s, a, n) {
                    0 == n && ($("#slide1 .banners").removeClass("vhidden").animateCss("fadeInUp"), $(".fp-prev").addClass("hideImp")),
                        1 == n && $("#slide2 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        2 == n && $("#slide3 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        3 == n && $("#slide4 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        4 == n && $("#slide5 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        5 == n && $("#slide6 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        6 == n && $("#slide7 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        7 == n && $("#slide8 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        8 == n && $("#slide9 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        9 == n && $("#slide10 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        10 == n && $("#slide11 .banners").removeClass("vhidden").animateCss("fadeInUp"),
                        11 == n &&
                        ($(".fp-prev").addClass("hideImp"),
                            $(".fp-next").addClass("hideImp"),
                            $(".fp-slidesNav").removeClass("active"),
                            $(".sidenav_trig").removeClass("active"),
                            $(".sidenav").addClass("active"),
                            $(".sidenav").mouseleave(function() {
                                $(this).addClass("active"), $(".sidenav_trig").removeClass("active");
                            })),
                        11 != n &&
                        $(".sidenav").mouseleave(function() {
                            $(this).removeClass("active"), $(".sidenav_trig").addClass("active");
                        });
                },
                onSlideLeave: function(e, s, a, n) {
                    10 == a &&
                        "right" == n &&
                        ($(".fp-prev").addClass("hideImp"),
                            $(".fp-next").addClass("hideImp"),
                            $(".fp-slidesNav").removeClass("active"),
                            $(".sidenav_trig").removeClass("active"),
                            $(".sidenav").addClass("active"),
                            $(".sidenav").mouseleave(function() {
                                $(this).addClass("active"), $(".sidenav_trig").removeClass("active");
                            })),
                        11 == a &&
                        ($(".fp-slidesNav").addClass("active"),
                            $(".fp-slidesContainer").addClass("slow"),
                            $(".fp-prev").removeClass("hideImp"),
                            $(".fp-next").removeClass("hideImp"),
                            setTimeout(function() {
                                $(".fp-slidesContainer").removeClass("slow");
                            }, 3500),
                            $(".sidenav_trig").addClass("active"),
                            $(".sidenav").removeClass("active")),
                        0 == a && "right" == n && $(".fp-prev").removeClass("hideImp"),
                        1 == a && "left" == n && $(".fp-prev").addClass("hideImp"),
                        0 == a && $("#slide1 .banners").addClass("vhidden"),
                        1 == a && $("#slide2 .banners").addClass("vhidden"),
                        2 == a && $("#slide3 .banners").addClass("vhidden"),
                        3 == a && $("#slide4 .banners").addClass("vhidden"),
                        4 == a && $("#slide5 .banners").addClass("vhidden"),
                        5 == a && $("#slide6 .banners").addClass("vhidden"),
                        6 == a && $("#slide7 .banners").addClass("vhidden"),
                        7 == a && $("#slide8 .banners").addClass("vhidden"),
                        8 == a && $("#slide9 .banners").addClass("vhidden"),
                        9 == a && $("#slide10 .banners").addClass("vhidden"),
                        10 == a && $("#slide11 .banners").addClass("vhidden");
                },
            }),
            $(".rmlink").click(function() {
                $(this)
                    .parent()
                    .parent()
                    .find(".readmore_content")
                    .slideToggle("slow", function() {
                        $(this).parent().find(".rmlink").toggleClass("active");
                    });
            }),
            $(".play-btn").click(function() {
                $(".logo_nav").animateCss("fadeOutUp", function() {
                        $(".logo_nav").addClass("vhidden");
                    }),
                    $(".top_links").animateCss("fadeOutUp", function() {
                        $(".top_links").addClass("vhidden");
                    }),
                    $(".search_container").animateCss("fadeOutUp", function() {
                        $(".search_container").addClass("vhidden");
                    }),
                    $(".dropd_top").animateCss("fadeOutUp", function() {
                        $(".dropd_top").addClass("vhidden");
                    }),
                    $(".sidenav").animateCss("fadeOut", function() {
                        $(".sidenav").addClass("vhidden");
                    }),
                    $(".vidbox").addClass("active"),
                    setTimeout(function() {
                        $("#myVideo")
                            .removeClass("vhidden")
                            .animateCss("fadeIn", function() {
                                document.getElementById("myVideo").play();
                            }),
                            $(".closevid").fadeIn();
                    }, 1e3);
            }),
            $(".closevid").click(function() {
                document.getElementById("myVideo").pause(),
                    $(".vidbox").removeClass("active"),
                    $(".logo_nav").removeClass("vhidden").animateCss("fadeInDown"),
                    $(".top_links").removeClass("vhidden").animateCss("fadeInDown"),
                    $(".search_container").removeClass("vhidden").animateCss("fadeInDown"),
                    $(".dropd_top").removeClass("vhidden").animateCss("fadeInDown"),
                    $(".sidenav").removeClass("vhidden").animateCss("fadeIn"),
                    $(".closevid").fadeOut();
            }),
            $(".nse_bse>a#one").click(function() {
                $(".nse_bse>a").removeClass("active"), $(this).addClass("active"), $(".box").removeClass("active"), $("#box_one").addClass("active");
            }),
            $(".nse_bse>a#two").click(function() {
                $(".nse_bse>a").removeClass("active"), $(this).addClass("active"), $(".box").removeClass("active"), $("#box_two").addClass("active");
            }),
            $(".foundation_grid a").hover(function() {
                $(this).toggleClass("up");
            }),
            $(".sidenav_trig").hover(function() {
                $(this).removeClass("active"), $(".sidenav").addClass("active");
            }),
            $(".business_grid a").hover(function() {
                $(this).children(".business_icons").toggleClass("active");
            });
    }),
    $(".back_to_top").click(function() {
        $.fn.fullpage.moveTo(1), $(".back_to_top").fadeOut();
    }),
    window.matchMedia("(max-width: 767px)").matches &&
    (applyOrientation(),
        (window.onresize = function(e) {
            applyOrientation();
        }));